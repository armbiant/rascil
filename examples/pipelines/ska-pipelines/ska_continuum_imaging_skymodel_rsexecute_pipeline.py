"""
Continuum processing pipeline
"""
# # Pipeline processing using Dask

results_dir = "./results/"

import logging

from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_func_python.imaging import create_image_from_visibility
from ska_sdp_func_python.image import image_gather_channels
from rascil.processing_components import create_visibility_from_ms
from rascil.workflows import (
    continuum_imaging_skymodel_list_rsexecute_workflow,
    weight_list_rsexecute_workflow,
)
from rascil.workflows.rsexecute.execution_support.rsexecute import rsexecute


def init_logging():
    logging.basicConfig(
        filename="%s/ska-pipeline.log" % results_dir,
        filemode="a",
        format="%(asctime)s.%(msecs)d %(name)s %(levelname)s %(message)s",
        datefmt="%H:%M:%S",
        level=logging.INFO,
    )


if __name__ == "__main__":
    log = logging.getLogger("rascil-logger")
    logging.info("Starting continuum imaging pipeline")

    rsexecute.set_client(use_dask=True)
    print(rsexecute.client)
    rsexecute.run(init_logging)

    nfreqwin = 8

    # Load data from previous simulation
    vis_list = [
        rsexecute.execute(create_visibility_from_ms)(
            r"%s/ska-pipeline_simulation_%d.ms" % (results_dir, v)
        )[0]
        for v in range(nfreqwin)
    ]

    vis_list = rsexecute.persist(vis_list)

    cellsize = 0.0003
    npixel = 1024
    pol_frame = PolarisationFrame("stokesI")

    model_list = [
        rsexecute.execute(create_image_from_visibility)(
            v, npixel=npixel, cellsize=cellsize, polarisation_frame=pol_frame
        )
        for v in vis_list
    ]

    model_list = rsexecute.persist(model_list)

    vis_list = weight_list_rsexecute_workflow(vis_list, model_list)

    imaging_context = "ng"

    continuum_imaging_list = continuum_imaging_skymodel_list_rsexecute_workflow(
        vis_list,
        model_imagelist=model_list,
        context=imaging_context,
        scales=[0],
        algorithm="mmclean",
        nmoment=2,
        niter=1000,
        fractional_threshold=0.3,
        threshold=0.1,
        nmajor=5,
        gain=0.25,
        deconvolve_facets=1,
        deconvolve_overlap=0,
        timeslice="auto",
        psf_support=128,
        restored_output="list",
    )

    log.info("About to run full graph")
    result = rsexecute.compute(continuum_imaging_list, sync=True)
    rsexecute.close()

    # The return is:
    #    (nchan * (residual_image, sumwt), nchan * restored_image, nchan * skymodel)
    residual = image_gather_channels([result[0][chan][0] for chan in range(nfreqwin)])
    restored = image_gather_channels([result[1][chan] for chan in range(nfreqwin)])
    deconvolved = image_gather_channels(
        [result[2][chan].image for chan in range(nfreqwin)]
    )

    log.info(deconvolved.image_acc.qa_image(context="Clean image cube"))
    deconvolved.image_acc.export_to_fits(
        "%s/ska-continuum-imaging_rsexecute_deconvolved_cube.fits" % (results_dir),
    )

    log.info(restored.image_acc.qa_image(context="Restored clean image cube"))
    restored.image_acc.export_to_fits(
        "%s/ska-continuum-imaging_rsexecute_restored_cube.fits" % (results_dir),
    )

    log.info(residual.image_acc.qa_image(context="Residual clean image cube"))
    residual.image_acc.export_to_fits(
        "%s/ska-continuum-imaging_rsexecute_residual_cube.fits" % (results_dir),
    )
