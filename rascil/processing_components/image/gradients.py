""" Image operations visible to the Execution Framework as Components
"""

__all__ = ["image_gradients"]

import warnings

from astropy.wcs import FITSFixedWarning

warnings.simplefilter("ignore", FITSFixedWarning)

import numpy
import logging

log = logging.getLogger("rascil-logger")

from ska_sdp_datamodels.image.image_model import Image


def image_gradients(im: Image):
    """Calculate image first order gradients numerically

    Two images are returned: one with respect to x and one with respect to y

    Gradient units are (incoming unit)/pixel e.g. Jy/beam/pixel

    :param im: Image
    :return: Gradient images
    """
    ##assert isinstance(im, Image)

    nchan, npol, ny, nx = im["pixels"].data.shape

    gradientx = Image.constructor(
        data=numpy.zeros_like(im["pixels"].data),
        polarisation_frame=im.image_acc.polarisation_frame,
        wcs=im.image_acc.wcs,
        clean_beam=im.attrs["clean_beam"],
    )
    gradientx["pixels"].data[..., :, 1:nx] = (
        im["pixels"].data[..., :, 1:nx] - im["pixels"].data[..., :, 0 : (nx - 1)]
    )
    gradienty = Image.constructor(
        data=numpy.zeros_like(im["pixels"].data),
        polarisation_frame=im.image_acc.polarisation_frame,
        wcs=im.image_acc.wcs,
        clean_beam=im.attrs["clean_beam"],
    )
    gradienty["pixels"].data[..., 1:ny, :] = (
        im["pixels"].data[..., 1:ny, :] - im["pixels"].data[..., 0 : (ny - 1), :]
    )

    return gradientx, gradienty
