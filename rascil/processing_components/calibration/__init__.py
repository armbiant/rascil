""" Calibration of observations, using single Jones matricies or chains of Jones matrices.

"""
from .iterators import *
from .operations import *
